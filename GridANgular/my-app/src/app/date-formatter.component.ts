import { Component } from '@angular/core';
import { Pipe, PipeTransform } from '@angular/core';
import { DatePipe, formatDate } from '@angular/common';

@Component({
    selector: 'app-date-formatter-cell',
    template: `
  
  <span>{{params.value | date:"MMMM dd, yyyy"}}</span>
  `
})
export class DateFormatterComponent {
    params: any;
    //today: number = Date.now();
    agInit(params: any): void {
        this.params = params;

    }
}
