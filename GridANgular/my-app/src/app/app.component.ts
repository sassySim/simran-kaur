import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { GridOptions, RowComp } from 'ag-grid-community';
import { concat } from 'rxjs';
import {NumberFormatterComponent} from './number-formatter.component';

import { AngularFrameworkComponentWrapper } from 'ag-grid-angular/lib/angularFrameworkComponentWrapper';
import { DateFormatterComponent } from './date-formatter.component';



@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
 
  employeeDetailsColumnDefs = [
    { headerName: 'Employee ID', field: 'EmpID', width: 150, sortable: true, filter: true  },
    { headerName: 'Name', field: 'FullName', width: 150, sortable: true, filter: true ,
      valueGetter:function(params){
        return params.data.FirstName+" "+params.data.MiddleName+" "+params.data.LastName;
      },
    },
    { headerName: 'Hire Date', field: 'Hire_Date' , width: 250, sortable: true, filter: true,
    cellRenderer:'dateFormatterComponent' 
  },
 
    {
      headerName: 'Salary',
      field: 'Salary',
      width:150,
      sortable:true,
      filter:true,
      editable: true,
      cellRenderer: 'numberFormatterComponent',
    },

    { headerName: 'Date Of Birth', field: 'DOB', width: 150, sortable: true, filter: true,cellRenderer:'dateFormatterComponent' },
    { headerName: 'Department ID', field: 'DeptID', width: 150, sortable: true, filter: true },
    { headerName: 'Manager ID', field: 'ManagerID', width: 150, sortable: true, filter: true },
    { headerName: 'Department', field: 'Department', width: 150, sortable: true, filter: true},       
   {headerName: 'Info', field: 'fieldName',width:90,
                cellRenderer : function(params){
                    return '<div><button  onClick=alert('+params.data.EmpID+')>View</button></div>';
                }
            }
   
  ];
  
  vehicleDetailsColumnDefs = [
    { headerName: 'Employee ID', field: 'EmpID', width: 150, sortable: true, filter: true },
    { headerName: 'Name', field: 'FullName', width: 150, sortable: true, filter: true ,
    valueGetter:function(params){
      return params.data.FirstName+" "+params.data.LastName;
    },
  },
    { headerName: 'Date Of Birth', field: 'DOB', width: 150, sortable: true, filter: true,
    cellRenderer:'dateFormatterComponent' },
    
    { headerName: 'Emp Id', field: 'EmpId1', width: 150, sortable: true, filter: true },
    { headerName: 'Due Amount', field: 'DueAmount', width: 150, sortable: true, filter: true ,
    cellRenderer: 'numberFormatterComponent'},
    { headerName: 'Vehicle ID', field: 'VehicleID', width: 150, sortable: true, filter: true }
  ];
  salaryDetailsColumnDefs = [
    { headerName: 'Employee ID', field: 'EmpID', width: 150, sortable: true, filter: true },
    { headerName: 'FirstName', field: 'FirstName', width: 150, sortable: true, filter: true },
    { headerName: 'Hire_Date', field: 'Hire_Date', width: 150, sortable: true, filter: true,
    cellRenderer:'dateFormatterComponent' },
    { headerName: 'Updated Salary', field: 'Salary', width: 150, sortable: true, filter: true ,
    cellRenderer: 'numberFormatterComponent'},
    { headerName: 'Salary', field: 'Salary1', width: 150, sortable: true, filter: true ,
    cellRenderer: 'numberFormatterComponent'},
    { headerName: 'Start Date', field: 'StartDate', width: 150, sortable: true, filter: true ,
    cellRenderer:'dateFormatterComponent' },
    { headerName: 'End Date', field: 'EndDate', width: 150, sortable: true, filter: true ,
    cellRenderer:'dateFormatterComponent' }
  ];
  loanDetailsColumnDefs = [
    { headerName: 'Employee ID', field: 'EmpId', width: 190, sortable: true, filter: true },
    { headerName: 'Due Amount', field: 'DueAmount', width: 190, sortable: true, filter: true ,
    cellRenderer: 'numberFormatterComponent'},
    { headerName: 'Bank Name', field: 'BankName', width: 190, sortable: true, filter: true }
  ];
  interestDetailsColumnDefs = [
    { headerName: 'Employee ID', field: 'EmpID', width: 200, sortable: true, filter: true },
    { headerName: 'Name', field: 'FullName', width: 150, sortable: true, filter: true ,
    valueGetter:function(params){
      return params.data.FirstName+" "+params.data.LastName;
    },
  },
    { headerName: 'Interest ID', field: 'InterestID', width: 200, sortable: true, filter: true },
    { headerName: 'Hobby Name', field: 'HobbyName', width: 200, sortable: true, filter: true },
  ];
  fullDetailsColumnDefs = [
    { headerName: 'Employee ID', field: 'EmpId', width: 200, sortable: true, filter: true },
    { headerName: 'Dept ID', field: 'DeptID', width: 200, sortable: true, filter: true },
    { headerName: 'Department', field: 'Department', width: 200, sortable: true, filter: true },
    { headerName: 'FullName', field: 'FullName', width: 150, sortable: true, filter: true ,
      valueGetter:function(params){
        return params.data.FirstName+" "+params.data.LastName;
      },
    },
    { headerName: 'Location', field: 'Location', width: 200, sortable: true, filter: true },
    { headerName: 'Manager ID', field: 'ManagerID', width: 200, sortable: true, filter: true },
    { headerName: 'Salary', field: 'Salary', width: 200, sortable: true, filter: true ,
    cellRenderer: 'numberFormatterComponent'},
    { headerName: 'Bank Name', field: 'BankName', width: 200, sortable: true, filter: true },
    { headerName: 'Hire Date', field: 'Hire_Date', width: 200, sortable: true, filter: true ,
    cellRenderer:'dateFormatterComponent' }
  ];
  drop() {
    console.log("hii");
    //alert("BUTTON CLICKEFD")
}
  employeeDetails: any;
  vehicleDetails: any;
  salaryDetails: any;
  loanDetails: any;
  interestDetails: any;
  fullDetails: any;

 
  frameworkComponents = {
    numberFormatterComponent: NumberFormatterComponent ,
    
    dateFormatterComponent:DateFormatterComponent
   
  };

  constructor(private http: HttpClient) {
  }
 
  ngOnInit() {

    this.employeeDetails = this.http.get('http://localhost:44333/api/Values/GetEmployeeDetails');
    this.vehicleDetails = this.http.get('http://localhost:44333/api/Values/GetVehicleDetails');
    this.salaryDetails = this.http.get('http://localhost:44333/api/Values/GetSalaryDetails');
    this.loanDetails = this.http.get('http://localhost:44333/api/Values/GetloanDetails');
    this.interestDetails = this.http.get('http://localhost:44333/api/Values/GetInterestDetails');
    this.fullDetails = this.http.get('http://localhost:44333/api/Values/GetFullDetails');
  }
}

