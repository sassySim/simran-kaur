import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {HttpClientModule} from '@angular/common/http';
import { AppComponent } from './app.component';
import { AgGridModule } from 'ag-grid-angular';
import { NumberFormatterComponent } from './number-formatter.component';
import { DateFormatterComponent } from './date-formatter.component';

@NgModule({
  declarations: [
    AppComponent,
    NumberFormatterComponent,
  
    DateFormatterComponent
    
  ],
  imports: [
    BrowserModule,HttpClientModule,
    AgGridModule.withComponents([NumberFormatterComponent,DateFormatterComponent])
  ],
  providers: [],
  
  bootstrap: [AppComponent]
})
export class AppModule { }
